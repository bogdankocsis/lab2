import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { Image } from "../entity/Image";
import { MnistData } from "../utilities/data";
import * as tf from "@tensorflow/tfjs-node"

export class ImageController {

  private classNames = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
  private imageRepository = getRepository(Image);

  async all(request: Request, response: Response, next: NextFunction) {
    return this.imageRepository.find();
  }

  async evaluate(request: Request, response: Response, next: NextFunction) {
    const data = new MnistData();
    await data.load(request.files.img);
    const model = await tf.loadLayersModel("file://src/utilities/model.json");

    const pred = this.doPrediction(model, data, 5)
    console.log(pred)
    const obj = new Image()
    obj.name = "image"
    obj.size = "256.00 bytes"
    obj.recognitionResult = pred[0].shape
    obj.url = "https://ibb.co/5GNXfnj"
    
    this.imageRepository.save(obj);
 
    return pred;
  }

  private doPrediction(model, data, testDataSize = 500) {
    const IMAGE_WIDTH = 28;
    const IMAGE_HEIGHT = 28;
    const testData = data.nextTestBatch(testDataSize);
    const testxs = testData.xs.reshape([testDataSize, IMAGE_WIDTH, IMAGE_HEIGHT, 1]);
    const labels = testData.labels.argMax(-1);
    const preds = model.predict(testxs).argMax(-1);

    testxs.dispose();
    return [preds, labels];
  }
}