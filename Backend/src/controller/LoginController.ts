import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";
import * as jwt from 'jsonwebtoken';

interface TokenData{
    token:string;
    expiresIn: number;
}

interface DataStoredInToken{
    _id:string;
}

export class LoginController {

    private userRepository = getRepository(User);

    async login(request: Request, response: Response, next: NextFunction) {
        let username: string = request.body.username;
        let password: string = request.body.password;

        const user = await this.userRepository.findOne(
            {   where:
                    {   username: username,
                        password: password
                    }
            });
          
        if(user) {
            const tokenData = this.createToken(user);
            return [{data: tokenData}];
        } else {
            return [{error: "Error"}];      
        }
    }

    private createToken(user:User): TokenData{
        const expiresIn=60*60;
        const secret = "octabiaadina";
        const dataStoredInToken:DataStoredInToken ={
            _id:String(user.id),
        };
        return {
            expiresIn,
            token: jwt.sign(dataStoredInToken, secret, {expiresIn}),
        };
    }
}