import { ImageController } from "./controller/ImageController";
import { LoginController } from "./controller/LoginController";
import { UserController } from "./controller/UserController";
import authMiddleware from "./middleware/auth";

export const Routes = [
    {
        method: "get",
        route: "/users",
        controller: UserController,
        action: "all",
        guard: authMiddleware
    }, {
        method: "get",
        route: "/users/:id",
        controller: UserController,
        action: "one",
        guard: authMiddleware
    }, {
        method: "post",
        route: "/users",
        controller: UserController,
        action: "save",
        guard: authMiddleware
    }, {
        method: "delete",
        route: "/users/:id",
        controller: UserController,
        action: "remove",
        guard: authMiddleware
    }, {
        method: "post",
        route: "/login",
        controller: LoginController,
        action: "login"
    }, {
        method: "get",
        route: "/images",
        controller: ImageController,
        action: "all",
        guard: authMiddleware
    }, {
        method: "post",
        route: "/evaluate",
        controller: ImageController,
        action: "evaluate",
    }, {
        method: "get",
        route: "/train",
        controller: ImageController,
        action: "train",
    }];