import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Routes } from "./routes";
import { User } from "./entity/User";
import { Image } from "./entity/Image";
import authMiddleware from "./middleware/auth";
import * as fileUpload from "express-fileupload";

createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());
    app.use(fileUpload({
        useTempFiles : false,
        tempFileDir : '/tmp/'
    }));
    // register express routes from defined application routes
    Routes.forEach(route => {
        if (route.guard)
            (app as any)[route.method](route.route, route.guard, (req: Request, res: Response, next: Function) => {
                const result = (new (route.controller as any))[route.action](req, res, next);
                if (result instanceof Promise) {
                    result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

                } else if (result !== null && result !== undefined) {
                    res.json(result);
                }
            });
        else
            (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
                const result = (new (route.controller as any))[route.action](req, res, next);
                if (result instanceof Promise) {
                    result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

                } else if (result !== null && result !== undefined) {
                    res.json(result);
                }
            });
    });

    // setup express app here
    // ...

    // start express server
    const server = app.listen(3000);

    // insert new users for test
    await connection.manager.save(connection.manager.create(User, {
        firstName: "Timber",
        lastName: "Saw",
        age: 27,
        username: "timber_saw",
        password: "12345678"
    }));

    // insert new images for test
    await connection.manager.save(connection.manager.create(Image, {
        name: "Eagle",
        size: "256.00 bytes",
        recognitionResult: "1.0079",
        url: "https://ibb.co/5GNXfnj"
    }));
    await connection.manager.save(connection.manager.create(Image, {
        name: "Artist",
        size: "256.00 bytes",
        recognitionResult: "4.0026",
        url: "https://ibb.co/5GNXfnj"
    }));
    await connection.manager.save(connection.manager.create(Image, {
        name: "Truth",
        size: "214.00 bytes",
        recognitionResult: "6.941",
        url: "https://ibb.co/5GNXfnj"
    }));
    await connection.manager.save(connection.manager.create(Image, {
        name: "Swim",
        size: "49.00 bytes",
        recognitionResult: "9.0122",
        url: "https://ibb.co/5GNXfnj"
    }));

    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => {
    console.log(error)
    process.on('uncaughtException', function () {

    })
});
