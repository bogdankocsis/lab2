import {NextFunction, Request, Response} from "express";
import * as jwt from 'jsonwebtoken';
import { nextTick } from "process";
import {getRepository} from "typeorm";
import { User } from "../entity/User";

interface DataStoredInToken{
    _id:string;
}
 
async function authMiddleware(request: Request, response: Response, next: NextFunction) {
    
  let userRepository = getRepository(User);

  if (request.headers.authorization) {
    const secret = "octabiaadina";
    try {
      const verificationResponse = jwt.verify(request.headers.authorization, secret) as DataStoredInToken;
      const id = verificationResponse._id;
      const user = await userRepository.findOne(id);
      if (user) {
        request.user = user;
        next()
      } else {
        response.status(401).send({error: "Error"})
        return response
      }
    } catch (error) {
        response.status(401).send({error: "Error"})
        return response
    }
  } else {
    response.status(401).send({error: "Error"})
    return response
  }

  // httpStatus.unahuthorized
  //response.status(401).send({error: {idk}})
}
 
export default authMiddleware;