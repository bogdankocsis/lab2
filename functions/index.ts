import { Datastore } from "@google-cloud/datastore";

// Creates a client
const datastore = new Datastore({
  projectId: 'angular-project-310718',
  keyFilename: 'angular-project-310718-7b96ba526e63.json'
});

// export function helloWorld(req, res) {
//   res.set('Access-Control-Allow-Origin', "*")
//   res.set('Access-Control-Allow-Methods', 'GET, POST')
//   res.set('Access-Control-Allow-Headers', "*");
//   const query = datastore.createQuery('user-log');
//   const logs = datastore.runQuery(query).then(entity => {
//     console.log(entity);
//     res.status(200).send(entity);
//   }).catch(err => {
//     console.error('ERROR:', err);
//     res.status(200).send(err);
//     return;
//   });

// };

  export function getPosts(req, res){
    res.set('Access-Control-Allow-Origin', "*")
    res.set('Access-Control-Allow-Methods', 'GET')
    res.set('Access-Control-Allow-Headers', '*');
    
    const query = datastore.createQuery('user-log');
    const logs = datastore.runQuery(query).then(entity => {
      console.log(entity);
      res.status(200).send(entity);
    }).catch(err => {
      console.error('ERROR:', err);
      res.status(200).send(err);
      return;
    });
  }
  

  export function addPost(req, res){
    res.set('Access-Control-Allow-Origin', "*")
    res.set('Access-Control-Allow-Methods', 'POST')
    res.set('Access-Control-Allow-Headers', '*');

    let title = req.query.title || req.body.title || 'default title';
    let author = req.query.author || req.body.author || 'default author';
    let body = req.query.body || req.body.body || 'default body';
    let datePost = req.query.datePost || req.body.datePost || new Date().toJSON();
  
    const taskKey = datastore.key('user-log');
    const entity = {
      key: taskKey,
      data: [
        {
          name: 'datePost',
          value: datePost,
        },
        {
          name: 'title',
          value: title,
          excludeFromIndexes: true,
        },
        {
          name: 'body',
          value: body,
          excludeFromIndexes: true,
        },
        {
          name: 'author',
          value: author,
          excludeFromIndexes: true,
        },
      ],
    };
  
    try {
      datastore.save(entity).then(entity => {
        console.log(entity);
        res.status(200).send(entity);
      }).catch(err => {
        console.error('ERROR:', err);
        res.status(200).send(err);
        return;
      });
      console.log(`Task ${taskKey.id} created successfully.`);
    } catch (err) {
      console.error('ERROR:', err);
    }
  }
  