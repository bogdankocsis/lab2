var datastore_1 = require("@google-cloud/datastore");
// Creates a client
var datastore = new datastore_1.Datastore({
    projectId: 'angular-project-310718',
    keyFilename: 'angular-project-310718-7b96ba526e63.json'
});
// export function helloWorld(req, res) {
//   res.set('Access-Control-Allow-Origin', "*")
//   res.set('Access-Control-Allow-Methods', 'GET, POST')
//   res.set('Access-Control-Allow-Headers', "*");
//   const query = datastore.createQuery('user-log');
//   const logs = datastore.runQuery(query).then(entity => {
//     console.log(entity);
//     res.status(200).send(entity);
//   }).catch(err => {
//     console.error('ERROR:', err);
//     res.status(200).send(err);
//     return;
//   });
// };
function getPosts(req, res) {
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', '*');
    var query = datastore.createQuery('user-log');
    var logs = datastore.runQuery(query).then(function (entity) {
        console.log(entity);
        res.status(200).send(entity);
    }).catch(function (err) {
        console.error('ERROR:', err);
        res.status(200).send(err);
        return;
    });
}
exports.getPosts = getPosts;
function addPost(req, res) {
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'POST');
    res.set('Access-Control-Allow-Headers', '*');
    var title = req.query.title || req.body.title || 'default title';
    var author = req.query.author || req.body.author || 'default author';
    var body = req.query.body || req.body.body || 'default body';
    var datePost = req.query.datePost || req.body.datePost || new Date().toJSON();
    var taskKey = datastore.key('user-log');
    var entity = {
        key: taskKey,
        data: [
            {
                name: 'datePost',
                value: datePost
            },
            {
                name: 'title',
                value: title,
                excludeFromIndexes: true
            },
            {
                name: 'body',
                value: body,
                excludeFromIndexes: true
            },
            {
                name: 'author',
                value: author,
                excludeFromIndexes: true
            },
        ]
    };
    try {
        datastore.save(entity).then(function (entity) {
            console.log(entity);
            res.status(200).send(entity);
        }).catch(function (err) {
            console.error('ERROR:', err);
            res.status(200).send(err);
            return;
        });
        console.log("Task " + taskKey.id + " created successfully.");
    }
    catch (err) {
        console.error('ERROR:', err);
    }
}
exports.addPost = addPost;
