import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './Guards/auth.guard';
import { ImageHistoryComponent } from './Pages/image-history/image-history.component';
import { LoginComponent } from './Pages/login/login.component';
import { PreviewComponent } from './Pages/preview/preview.component';
import { UploadImageComponent } from './Pages/upload-image/upload-image.component';
import { StorageService } from './Services/storage.service';

const routes: Routes = [

  { path: '', component: LoginComponent },
  { path: 'upload', component: UploadImageComponent, canActivate: [AuthGuard] },
  { path: 'history', component: ImageHistoryComponent, canActivate: [AuthGuard] },
  {path: 'preview', component: PreviewComponent},
  { path: '**', redirectTo: ' ' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
