import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, ObservableInput, throwError } from 'rxjs';
import { catchError, retry, take } from 'rxjs/operators';
import { StorageService } from './storage.service';


@Injectable({
  providedIn: 'root'
})
export class RestRequestService {

  constructor(private serviceToken: StorageService, private http: HttpClient) {
    this.serviceToken = new StorageService()
  }

  get(URL: string, token: string): Observable<any> {
    //this.serviceToken.setToken(token)
    return this.http.get(URL, { headers: this.getHeaders() }).pipe(catchError(this.checkAuth))
  }

  post(URL: string, data: any): Observable<any> {
    return this.http.post(URL, data, { headers: this.getHeaders() }).pipe(catchError(this.checkAuth));
  }

  getHeaders() {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('Access-Control-Allow-Origin', '*');
    headers = headers.set('Authorization', this.serviceToken.getToken());
    return headers;
  }

  checkAuth(error: HttpErrorResponse) : ObservableInput<any> {
    if(error.status == 401){
      this.serviceToken.setToken("")
    }
    return throwError('Something bad happened; please try again later.');
  }
}