import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { User } from 'src/app/Entities/user';

@Injectable({
  providedIn: 'root'
})
export class HeaderStateService {

  public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  constructor(private router: Router) { }

  // login(user : User){
  //   if(user.username !== '' && user.password !== ''){
  //     this.loggedIn.next(true);
  //     this.router.navigate(['/history']);
  //   }
  // }

  // logout(){
  //   this.router.navigate(['/']);
  //   this.loggedIn.next(false);

  // }
}
