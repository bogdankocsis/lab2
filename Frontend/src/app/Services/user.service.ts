import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HeaderStateService } from './header-state.service';
import { RestRequestService } from './rest-request.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private storageService: StorageService, private request: RestRequestService, private header : HeaderStateService) { 
  }

  login(body: any): Observable<any>{
    return this.request.post("http://localhost:3000/login", body).pipe(tap((resp: any) => {
					if(resp[0].data){
            this.storageService.setToken(resp[0].data.token);
            this.header.loggedIn.next(true);
          }
          else{
            this.header.loggedIn.next(false);
          }
    
    }));
  }

  images(): Observable<any>{
    return this.request.get("http://localhost:3000/images", this.storageService.getToken()).pipe(tap((resp: any) => {
      this.header.loggedIn.next(true);
    }));
  }
}
