import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { HeaderStateService } from 'src/app/Services/header-state.service';
import { StorageService } from '../Services/storage.service';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private serviceStorage: StorageService, private authService: HeaderStateService, private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.serviceStorage.getToken() == "") {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}