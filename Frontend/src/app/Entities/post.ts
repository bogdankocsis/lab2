export interface Post {
    title: string,
    body: string,
    author: string,
    datePost: Date
}
