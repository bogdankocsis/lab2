import { Component, OnInit } from '@angular/core';
import { observable, Observable, of } from 'rxjs';
import { HeaderStateService } from 'src/app/Services/header-state.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn$: Observable<boolean> ;

  constructor(private headerService: HeaderStateService) {
    this.isLoggedIn$ = of(false);
   }

  ngOnInit(): void {
    this.isLoggedIn$ = this.headerService.isLoggedIn;
  }

  onLogout(){
    //this.headerService.logout();
  }

}
