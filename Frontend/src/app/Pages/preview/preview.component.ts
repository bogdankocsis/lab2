import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Post } from 'src/app/Entities/post';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  constructor(private http: HttpClient, public dialog: MatDialog) { }

  result = {}
  postArray!: Post[];
  postObject: Post = {
    title: '',
    body: '',
    author: '',
    datePost: new Date()
  }
  ngOnInit(): void {
    this.get();
    console.log(this.postArray)
  }


  get() {
    this.http.get('https://us-central1-angular-project-310718.cloudfunctions.net/getPosts').subscribe({
      next: (val: any) => { this.postArray = val[0]; },
      error: (error: any) => console.error(error)
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddPost, {
      width: '500px',
      data: {
        title: this.postObject.title,
        body: this.postObject.body,
        author: this.postObject.author,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.postObject = {
          ...result,
          datePost: new Date().toJSON()
        };

        this.postArray.push(this.postObject);
        this.http.post<Post>('https://us-central1-angular-project-310718.cloudfunctions.net/addPost', this.postArray).subscribe(
          (val) => console.log(val)
        ) 
      }
    });
  }


}

@Component({
  selector: 'add-post',
  templateUrl: 'add-post.html',
})

export class AddPost {

  constructor(
    public dialogRef: MatDialogRef<AddPost>,
    @Inject(MAT_DIALOG_DATA) public data: Post) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
