import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import{Router} from '@angular/router'
import { HeaderStateService } from 'src/app/Services/header-state.service';
import { UserService } from 'src/app/Services/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  title = "User-Login";
  submitted = true

  loginForm = this.fb.group({
    username : ['',[Validators.required, Validators.minLength(5)]],
    password : ['', [Validators.required,Validators.minLength(8)]],
  });


	constructor(  private router:Router,
				private fb: FormBuilder, 
				private userService: UserService,
				private headerState: HeaderStateService) { }

  ngOnInit(): void {
  }


	validate() {
		if (this.loginForm.valid)
		{ 
			this.userService.login(
				{username: this.loginForm.controls['username'].value, password: this.loginForm.controls['password'].value})
				.subscribe(data => {
					if(data[0].data){
						this.router.navigate(['/history']);
					}
					else{
						this.router.navigate(['/']);
					}
			}, error => {})
		}
		
	}

}
