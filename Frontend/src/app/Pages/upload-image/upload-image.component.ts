import { Component, OnInit } from '@angular/core';
import { ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { FileUploadService } from 'src/app/file-upload.servce';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss']
})
export class UploadImageComponent implements OnInit {
  
  private fileToUpload: any;

  constructor( public fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private fileUpload: FileUploadService) {
     }

  ngOnInit(): void {
  }
    /*##################### Registration Form #####################*/
    registrationForm = this.fb.group({
      file: [null]
    })  
  
    /*########################## File Upload ########################*/
    @ViewChild('fileInput') el: ElementRef | undefined;
    imageUrl: any = 'https://purepng.com/public/uploads/large/purepng.com-ninjashinobininjacovert-agentassassinationguerrilla-warfaresamuraiclip-art-1421526960629zucts.png';
    editFile: boolean = true;
    removeUpload: boolean = false;
  
    uploadFile(event: any) {
      this.fileToUpload = event.target.files.item(0) as File;
      console.log(this.fileToUpload)

      this.fileUpload.postFile(this.fileToUpload).subscribe(data => {
          console.log('Success');
        }, error => {
          console.log(error);
        });


      let reader = new FileReader(); // HTML5 FileReader API
      let file = event.target.files[0];
      if (event.target.files && event.target.files[0]) {
        reader.readAsDataURL(file);
  
        // When file uploads set it to file formcontrol
        reader.onload = () => {
          this.imageUrl = reader.result;
          this.registrationForm.patchValue({
            file: reader.result
          });
          this.editFile = false;
          this.removeUpload = true;
        }
        // ChangeDetectorRef since file is loading outside the zone
        this.cd.markForCheck();        
      }
    }
  
    
    


}
