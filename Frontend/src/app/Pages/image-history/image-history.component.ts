import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { FileSizePipe } from 'src/app/Pipes/file-size.pipe';
import { RestRequestService } from 'src/app/Services/rest-request.service';
import { UserService } from 'src/app/Services/user.service';

export interface ImageDetail {
  id: number;
  name: string;
  size: string;
  recognitionResult: string;
  url: string;
}

const ELEMENT_DATA: ImageDetail[] = [];


const ELEMENT_DATA2: ImageDetail[] = [
  { id: 1, size: "256", name: 'Eagle', recognitionResult: "1234", url: 'https://ibb.co/5GNXfnj' },
  { id: 1, size: "256", name: 'Eagle', recognitionResult: "1234", url: 'https://ibb.co/5GNXfnj' },
  { id: 1, size: "256", name: 'Eagle', recognitionResult: "1234", url: 'https://ibb.co/5GNXfnj' },
  { id: 1, size: "256", name: 'Eagle', recognitionResult: "1234", url: 'https://ibb.co/5GNXfnj' },
  { id: 1, size: "256", name: 'Eagle', recognitionResult: "1234", url: 'https://ibb.co/5GNXfnj' },
  { id: 1, size: "256", name: 'Eagle', recognitionResult: "1234", url: 'https://ibb.co/5GNXfnj' },
  { id: 1, size: "256", name: 'Eagle', recognitionResult: "1234", url: 'https://ibb.co/5GNXfnj' },
  { id: 1, size: "256", name: 'Eagle', recognitionResult: "1234", url: 'https://ibb.co/5GNXfnj' },
];


@Component({
  selector: 'app-image-history',
  templateUrl: './image-history.component.html',
  styleUrls: ['./image-history.component.scss'],
  providers: [FileSizePipe]
})
export class ImageHistoryComponent implements OnInit {

  displayedColumns: string[] = ['select', 'id', 'image name', 'size', 'recognition result', 'image download link'];
  selection = new SelectionModel<ImageDetail>(true, []);
  dataSource = new MatTableDataSource<ImageDetail>(ELEMENT_DATA2);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ImageDetail): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.size + 1}`;
  }

  constructor(private datasize: FileSizePipe,
    private rest: RestRequestService,
    private userService: UserService,) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.userService.images().subscribe(data => {
      data.forEach((element: ImageDetail) => {
        ELEMENT_DATA.push(element);
      });
      this.dataSource.data = ELEMENT_DATA;
    }, error => { })
  }
}
