import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, take, tap } from 'rxjs/operators';
import { StorageService } from './Services/storage.service';
import { RestRequestService } from './Services/rest-request.service';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private httpClient: HttpClient) { }

  postFile(fileToUpload: File): Observable<Object> {
    const endpoint = 'http://localhost:3000/evaluate';
    const formData: FormData = new FormData();
    formData.append('img', fileToUpload, fileToUpload.name);
    return this.httpClient.post(endpoint, formData).pipe(take(1), tap((resp: any) => {
        console.log(resp);
    }))
    }
}