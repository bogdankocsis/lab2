import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, take } from 'rxjs/operators';
import { StorageService } from './Services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class RestRequestService {

  constructor(private http: HttpClient, private storage: StorageService) { }

  get(URL: string, token: string): Observable<any> {
    return this.http.get(URL, {headers: this.getHeaders()}).pipe(take(1))
  }

  post(URL: string, data: any, ): Observable<any> {
    return this.http.post(URL, data, {headers: this.getHeaders()}).pipe(take(1));
  }

  getHeaders() {
    const headers = { Authorization: "kjhgfdfgyui" };
    return headers;
  }
}